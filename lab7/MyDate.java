public class MyDate {

    private int day,month,year;
    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month-1;
        this.year = year;
    }

    public void incrementDay() {
        incrementDay(1);
    }

    public void incrementDay(int day) {
        int newDay = day+this.day;
        int maxDay = maxDays[month];
        if(newDay > maxDay){
            incrementMonth();
            this.day = maxDay-newDay;
        }
        else if(month == 1 && newDay ==29 && !leapYear()){
            this.day = 1;
            incrementMonth();
        }
        else {
            this.day = newDay;
        }
    }

    private boolean leapYear(){
        return (year % 4==0 ? true : false);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public void incrementYear(int diff) {
        year+=diff;
    }

    public void decrementDay() {
        int newDay = day+1;
        if(newDay==0){
            day = 31;
            decrementMonth();
        }else{
            day = newDay;
        }
    }

    public void decrementDay(int day) {
        int newDay = this.day-day;
        int maxDay = maxDays[month-1];
        if(newDay<=0) {
            decrementMonth();
            this.day = maxDay - newDay;
        }
        else{
            this.day = newDay;
        }
    }

    public void decrementYear() {
        decrementYear(1);
    }

    public void decrementYear(int year) {
        this.year -= year;
    }

    public void decrementMonth() {
        decrementMonth(1);
    }

    public void decrementMonth(int month) {
        this.month -= month;
        if(this.month<0){
            decrementYear();
            this.month = 11+ this.month;
        }
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementMonth(int diff) {
        int newMonth = (month+diff) % 12;
        int yearDiff = (month+diff)/12;
        month = newMonth;
        year+=yearDiff;
    }

    public boolean isBefore(MyDate anotherDate) {
        return false;
    }

    public boolean isAfter(MyDate anotherDate) {
        return false;
    }

    public int dayDifference(MyDate anotherDate) {
        return 0;
    }





    public String toString(){
        return year+"-"+((month+1<10) ? "0" : "")+(month+1)+"-"+((day<10) ? "0" : "")+day;
    }
}
