import java.io.IOException;
import java.util.Scanner;
public class TicTacToe {
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		boolean turn = true;
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
		boolean winCheck = false;
		while (true) {
			if (turn) {
				printBoard(board);
				System.out.print("Player 1 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				int col = reader.nextInt();
				if (row < 4 && row > 0 && col < 4 && col > 0 && board[row - 1][col - 1] == ' ') {
					board[row - 1][col - 1] = 'X';
					turn = false;
					winCheck = checkBoard(board, row-1 , col-1);
				} else
					System.out.println("You entered wrong location.");
				if (winCheck){
					printBoard(board);
					System.out.println("Congratulations, Player 1 won.");
					break;
				}
			} else {
				printBoard(board);
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();
				if (row < 4 && row > 0 && col < 4 && col > 0 && board[row - 1][col - 1] == ' ') {
					board[row - 1][col - 1] = 'O';
					turn = true;
					winCheck = checkBoard(board, row-1 , col-1);
				} else
					System.out.println("You entered wrong location.");

				if (winCheck){
					printBoard(board);
					System.out.println("Congratulations, Player 2 won.");
					break;
				}

			}

		}

	}
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");
		}
	}
	public static boolean checkBoard(char[][] board, int row , int col){
		boolean check1;
		boolean check2;
		boolean check3 = false;
		boolean check4 = false;
		boolean check5 = false;
		check1 = board[row][col]==board[row][0] && board[row][col]==board[row][1] && board[row][col]==board[row][2];
		check2 = board[row][col]==board[0][col] && board[row][col]==board[1][col] && board[row][col]==board[2][col];
		if((row==0 && col==0)||(row==2 && col==2))
			check3 = board[row][col]==board[1][1] && board[row][col]==board[0][0];
		if(row==0 && col==2)
			check4 = board[row][col]==board[1][1] && board[row][col]==board[2][0];
		if(row==2 && col==0)
			check5 = board[row][col]==board[1][1] && board[row][col]==board[0][2];
		if(check1 || check2 || check3 || check4 || check5)
			return true;
		else
			return false;
	}
}