package shapes3d;

public class TestShapes3d {
    public static void main(String[] args){
        Cylinder cy = new Cylinder(5,10);
        Cube cube = new Cube(4);
        System.out.println(cy);
        System.out.println("Volume= "+cy.volume());
        System.out.println("Cylinder Area= "+cy.area());

        System.out.println("cube volume= "+cube.volume());
        System.out.println("cube area= "+cube.area());
    }
}
