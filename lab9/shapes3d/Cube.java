package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    private double side;

    @Override
    public String toString() {
        return "Cube{" +
                "side=" + side +", area="+area()+", volume="+volume()+
                "} " + super.toString();
    }

    public double volume(){
        return super.area()*side;
    }

    public double area(){
        return 6*super.area();
    }

    public Cube(double side) {
        super(side);
    }


}
