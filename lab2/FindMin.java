public class FindMin{

	public static void main(String[] args){
		if (args.length==3){
			int a = Integer.parseInt(args[0]);
			int b = Integer.parseInt(args[1]);
			int c = Integer.parseInt(args[2]);
        		boolean someCondition = a < b;
			int min= someCondition ? a : b;
			someCondition = min < c;
			min= someCondition ? min : c;
			System.out.println("Minimum = "+min);
		}
	
	}

}